import {extendTheme} from '@chakra-ui/react';

export const theme = extendTheme({
    colors:{
        black:{
            '800': '#000000',
            '700': '160E0E',
            '100': '#E5E5E5'
        },

        gray:{
            '100': '#868484'
        },

        blue:{
            '100': '#1979AF'
        },

        green:{
            '100': '#56C14C'
        },

        red:{
            '100': '#D20B0B'
        }

    },

    styles:{
        global:{
            body:{
                bg: 'gray.100'
            }
        }
    }
})