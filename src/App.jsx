import { useState } from "react";
import { Calculadora } from "./components/Calculadora";
export function App() {
  return (
    <>
      <h1>Calculadora</h1>
      <Calculadora />
    </>
  );
}
