import { Botao } from '../Botao'
import { Flex, Box, Text, Stack, Button, ButtonGroup } from '@chakra-ui/react'
import { VisorNumerico } from '../VisorNumerico'
import { useState } from 'react'
export function Calculadora() {

  const [num, setNum] = useState(0);

  //função que recebe valor digitado ao clicar no botao
  function inputNum(e) {
    var input = e.target.value;
    setNum(num + input)
  }

  //função para zerar setNum
  function clear(){
    setNum(0)
  }

  return (
    <>
      <VisorNumerico />
      <Flex align="center" justify="center">
        <Box bg="gray.400" w="400px" h="5rem">
          <Flex justifyContent='right' mt="3.5rem">
            <Text align='center' justifyContent='right'>{num}</Text>
          </Flex>
        </Box>
      </Flex>

      <Botao />
      <Flex align='center' justify='center' m='2rem'>
        <Stack direction='column' >

          <ButtonGroup gap='4'>
            <Button bg='gray.500' onClick={inputNum} value={7}>7</Button>
            <Button bg='gray.500' onClick={inputNum} value={8}>8</Button>
            <Button bg='gray.500' onClick={inputNum} value={9}>9</Button>
            <Button bg='blue.500'>+</Button>
          </ButtonGroup>

          <ButtonGroup gap='4'>
            <Button bg='gray.500' onClick={inputNum} value={4}>4</Button>
            <Button bg='gray.500' onClick={inputNum} value={5}>5</Button>
            <Button bg='gray.500' onClick={inputNum} value={6}>6</Button>
            <Button bg='blue.400'>-</Button>
          </ButtonGroup>

          <ButtonGroup gap='4'>
            <Button bg='gray.500' onClick={inputNum} value={1}>1</Button>
            <Button bg='gray.500' onClick={inputNum} value={2}>2</Button>
            <Button bg='gray.500' onClick={inputNum} value={3}>3</Button>
            <Button bg='blue.400'>/</Button>
          </ButtonGroup>

          <ButtonGroup gap='4'>

            <Button bg='gray.500' onClick={inputNum} value={0}>0</Button>
            <Button bg='green.500'>=</Button>
            <Button bg='blue.400'>*</Button>
            <Button bg='blue.400'>%</Button>


          </ButtonGroup>
          <ButtonGroup gap='4'>
            <Button bg='red.600' onClick={clear}>X</Button>
            <Button bg='blue.400'>+/-</Button>
          </ButtonGroup>
        </Stack>
      </Flex>
    </>
  )
}
